<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<title>Show All Questions</title>
</head>
<body>
    <table border=1>
        <thead>
            <tr>
                <th>ID</th>
                <th>Question Number</th>
                <th>Question Text</th>
                <th>Image Location</th>
                <th colspan=2>Action</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${questions}" var="question">
                <tr>
                    <td><c:out value="${question.questionId}" /></td>
                    <td><c:out value="${question.questionNumber}" /></td>
                    <td><c:out value="${question.questionText}" /></td>
                    <td><c:out value="${question.imageLocation}" /></td>
                    <td><a href="QuestionController?action=edit&questionId=<c:out value="${question.questionId}"/>">Update</a></td>
                    <td><a href="QuestionController?action=delete&questionId=<c:out value="${question.questionId}"/>">Delete</a></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    <p><a href="QuestionController?action=insert">Add Question</a></p>
</body>
</html>