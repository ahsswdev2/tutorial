package org.ahs.controller;

//cccc
//comment to test git

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.ahs.dao.QuestionDao;
import org.ahs.model.Question;


public class QuestionController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static String INSERT_OR_EDIT = "/question.jsp";
    private static String LIST_QUESTION = "/listQuestion.jsp";
    private QuestionDao dao;

    public QuestionController() {
        super();
        dao = new QuestionDao();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String forward="";
        String action = request.getParameter("action");

        if (action.equalsIgnoreCase("delete")){
        	int questionId = Integer.parseInt(request.getParameter("questionId"));
            dao.deleteQuestion(questionId);
            forward = LIST_QUESTION;
            request.setAttribute("questions", dao.getAllQuestions());    
        } else if (action.equalsIgnoreCase("edit")){
            forward = INSERT_OR_EDIT;
            int questionId = Integer.parseInt(request.getParameter("questionId"));
            Question question = dao.getQuestionById(questionId);
            request.setAttribute("question", question);
        } else if (action.equalsIgnoreCase("listQuestion")){
            forward = LIST_QUESTION;
            request.setAttribute("questions", dao.getAllQuestions());
        } else {
            forward = INSERT_OR_EDIT;
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Question question = new Question();
        question.setQuestionNumber(Integer.parseInt(request.getParameter("questionNumber")));
        question.setQuestionText(request.getParameter("questionText"));
        question.setImageLocation(request.getParameter("imageLocation"));
        String id = request.getParameter("questionId");
        
        if(id == null || id.isEmpty())
        {
        	dao.addQuestion(question);
        }
        else
        {
            question.setQuestionId(Integer.parseInt(id));
            dao.updateQuestion(question);
        }
        RequestDispatcher view = request.getRequestDispatcher(LIST_QUESTION);
        request.setAttribute("questions", dao.getAllQuestions());
        view.forward(request, response);
    }
}